/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/bottommenu/pages/downloading_page.dart';
import 'package:elRepoIo/ui/common/postsTeaser.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:flutter/material.dart';
import 'package:elRepoIo/ui.dart' show BrowseXScreen;

class BookmarksScreen extends StatefulWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => BookmarksScreen(),
  );

  @override
  _BookmarksScreenState createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends State<BookmarksScreen> {

  Future<List<dynamic>> _userPostHeaders() =>
      repo.getPostHeaders("USER", rs.authIdentityId);


  Future<List<dynamic>> _bookmarksPostHeaders() =>
      repo.getBookmarks();

  Future<List<dynamic>> _downloadingPostHeaders() =>
      repo.getBookmarks(downloading: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      body:
      BrowseXScreen(myTabs: {
        'Your content': PostTeasersUpdatable(generator: _userPostHeaders, linkPosts: true),
        'Downloaded' : PostTeasersUpdatable(generator:  _bookmarksPostHeaders, linkPosts: false,),
        'In progress': PostTeasersUpdatable(generator: _downloadingPostHeaders, linkPosts: false),
      }),
    );
  }
}

