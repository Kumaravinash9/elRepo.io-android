/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/downloadBar.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/ui/common/updatableFutureBuilder.dart';
import 'package:flutter/material.dart';
import 'package:elRepoIo/ui.dart' show TagArguments;


/// @deprecated This Widget return a list of downloading files witha  downloading
/// progress bar.
///
/// We use it during the develop of local tab screen but finally we decided to
/// use a post teaser to show donwloading content
///
/// Save it here for use it on the future if needed.
class DownloadingPage extends StatefulWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => DownloadingPage(),
  );

  @override
  _DownloadingPageState createState() => _DownloadingPageState();
}

class _DownloadingPageState extends State<DownloadingPage> {

  Future<List<dynamic>> _getTags() =>
      repo.getDownloadingListDetails();

  @override
  Widget build(BuildContext context) {
    return UpdatableFutureBuilder(
        generator: _getTags,
        callbackWidget: (context, data, updateFunction) =>
            _downloadingCard(context, data)
    );
  }
}

Widget _downloadingCard(context, dynamic data,) {
  print("data");
  print(data);
  return
    Column(
      children: [
        ListTile(
          title: InkWell(
              onTap: () {
                print("tapped on $data");
              },
              child: Text(data["fname"],
                  style: TextStyle(color: REMOTE_COLOR))),
        ),
        DownloadBar(downloadingHash: data["hash"])
      ],
    );
}
