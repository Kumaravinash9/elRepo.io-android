/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/common/postsTeaser.dart';
import 'package:elRepoIo/ui/common/searchBar.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:flutter/material.dart';

class ExploreScreen extends StatefulWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => ExploreScreen(),
  );

  @override
  _ExploreScreenState createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen> {

  Future<List<dynamic>> _getPostHeaders() =>
      repo.exploreContent();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(32),
          ),
//        child: SingleChildScrollView(
          child:
              PostTeasersUpdatable(generator:  _getPostHeaders),
//        ),
        )
    );
  }
}

