/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/bottommenu/pages/explore_page.dart';
import 'package:elRepoIo/ui/bottommenu/pages/tags_page.dart';
import 'package:elRepoIo/ui/bottommenu/pages/following_page.dart';
import 'package:elRepoIo/ui/bottommenu/pages/bookmarks_page.dart';
import 'package:flutter/material.dart';
import '../../../ui.dart';

class TabNavigationItem {
  final Widget page;
  final Widget title;
  final Icon icon;

  TabNavigationItem({
    @required this.page,
    @required this.title,
    @required this.icon,
  });

  static List<TabNavigationItem> get items => [
    TabNavigationItem(
      page: PublishScreen(),
      icon: Icon(Icons.add_circle_rounded),
      title: Text("Publish"),
    ),
    TabNavigationItem(
      page: BookmarksScreen(),
      icon: Icon(Icons.local_library_rounded),
      title: Text("Local"),
    ),
    TabNavigationItem(
      page: BrowseXScreen(myTabs: {
        'Explore' : ExploreScreen(),
        'Tags': TagsPage(),
        'Following': FollowingPage(),
      }),
      icon: Icon(Icons.explore),
      title: Text("Discover"),
    ),
  ];
}
