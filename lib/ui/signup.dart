/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,
      body: Container(
        //margin: defaultMargin(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/bg1.jpg"),
                    fit: BoxFit.cover,
                    alignment: Alignment.bottomCenter,
                  ),
                ),
              ),
            ),
            new SignUpForm(),
          ],
        ),
      ),
      /*new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new SignUpForm()]),
      ),
      */
    );
  }
}

class SignUpForm extends StatefulWidget {
  SignUpForm({Key key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String locationName;
    String password;
    return Form(
      key: _formKey,
      child: Expanded(
        flex: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 12),
                //TODO refactor "menu", signup and bckp  (btn)
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      (IntlMsg.of(context).signUp),
                      style: TextStyle(
                          color: NEUTRAL_COLOR,
                          fontWeight: FontWeight.bold,
                          fontSize: 21),
                    ),
                    InkWell(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: REMOTE_COLOR, // background
                          onPrimary: WHITE_COLOR, // foreground
                        ),
                        child: Text(
                          "Tengo un Backup",
                          style: TextStyle(color: WHITE_COLOR),
                        ),
                        onPressed: () async {
                          final params =
                              fileDialog.OpenFileDialogParams(localOnly: true);
                          final backupFilePath =
                              await fileDialog.FlutterFileDialog.pickFile(
                                  params: params);
                          final zipFile = File(backupFilePath);
                          final rsPath = await getRetrosharePath();
                          final rsDir = Directory(rsPath);

                          rsDir.list(recursive: false).forEach((f) {
                            print(f);
                          });

                          await rscontrol.stopRetroshare();

                          rsDir.deleteSync(recursive: true);
                          rsDir.createSync();

                          try {
                            archive.ZipFile.extractToDirectory(
                                    zipFile: zipFile, destinationDir: rsDir)
                                .then((exitCode) {
                              rscontrol.startRetroshare().then((exitCode) {
                                repo.getAccountStatus().then((accountStatus) {
                                  if (accountStatus ==
                                      cnst.AccountStatus.hasLocation) {
                                    Navigator.pushReplacementNamed(
                                        context, '/login');
                                  }
                                });
                              });
                            });
                          } catch (e) {
                            print(e);
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 36),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.person,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: IntlMsg.of(context).username,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return IntlMsg.of(context).usernameCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => locationName = val,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.lock,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: IntlMsg.of(context).password,
                          suffix: InkWell(
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: LOCAL_COLOR,
                            ),
                            onTap: _toggle,
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return IntlMsg.of(context).passwordCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => password = val,
                        obscureText: _obscureText,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 17),
                  child: RaisedButton(
                    onPressed: () async {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      final form = _formKey.currentState;
                      if (form.validate()) {
                        form.save();
                        //TODO show an status that let users know that their account is creating
                        showLoadingDialog(
                            context, IntlMsg.of(context).creatingAccount);
                        Navigator.of(context).pushNamed('/onboarding');
                        final location =
                            await repo.signUp(password, locationName);

                        // we log in the account then stop RetroShare after id creation so data is persisted to avoid
                        // problems if the app crashes during the first run.
                        await repo.login(password, location);
                        await rscontrol.stopRetroshare();

                        rscontrol.startRetroshare().then((exitCode) {
                          Navigator.pushReplacementNamed(context, '/login');
                        });

//                        await rscontrol.prepareSystem();
//                        utils.initConnectivity();
                      }
                    },
                    child:
                        Center(child: Text(IntlMsg.of(context).createAccount)),
                    color: LOCAL_COLOR,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
