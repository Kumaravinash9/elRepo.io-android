/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,

      body: new Container(
       // margin: defaultMargin(),
        child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/bg1.jpg"),
                      fit: BoxFit.cover,
                      alignment: Alignment.bottomCenter,
                    ),
                  ),
                ),
              ),
              new LoginForm()
            ]
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool _obscureText = true;
  //String _password;

  Map location;

  @override
  void initState() {
    rs.RsLoginHelper.getDefaultLocation().then(
            (loc) {
          setState(() {
            location = loc;
          });
        });
    super.initState();
  }

  void _toggle(){
    setState((){
      _obscureText = !_obscureText;
    });
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String password;
    return Form(
      key: _formKey,
      child: Expanded(
        flex: 3,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Iniciar Sesión:",
                      style: TextStyle(fontSize: 22.0),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      location == null ? "" : location["mLocationName"],
                      style: TextStyle(
                          fontSize: 20.0
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child:
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.lock,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: IntlMsg.of(context).password,
                          suffix: InkWell(
                            child: Icon(
                                _obscureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                              color: LOCAL_COLOR,
                            ),
                            onTap: _toggle,
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return IntlMsg.of(context).passwordCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => password = val,
                        obscureText: _obscureText,

                      ),

                    ),
                  ],
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: RaisedButton(
                    onPressed: () async {
                      final form = _formKey.currentState;
                      showLoadingDialog(context, IntlMsg.of(context).loggingIn);
                      if (form.validate()) {
                        form.save();
                        final responseStatus = await repo.login(password, location);

                        if (responseStatus != 0) {
                          final snackBar = SnackBar(
                            content: Text(IntlMsg.of(context).loginFailed),
                            duration: Duration(seconds: 6),
                          );
                          Navigator.pop(context); //pop dialog
                          Scaffold.of(context).showSnackBar(snackBar);
                          print("Bad login attempt.");
                          return;
                        }

                        await rscontrol.prepareSystem();
                        rscontrol.setControlCallbacks();
                        // we initialize the connectivity settings after RS login so we can set rates.
                        utils.initConnectivity();

                        Navigator.pop(context); //pop dialog
                        Navigator.pushReplacementNamed(context, '/main');
                      }
                    },
                    child: Center(child: Text(IntlMsg.of(context).login)),
                    color: LOCAL_COLOR,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
