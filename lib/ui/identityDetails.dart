/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class IdentityDetailsScreen extends StatefulWidget {
  final IdentityArguments args;
  IdentityDetailsScreen(this.args);

  @override
  _IdentityDetailsScreenState createState() => _IdentityDetailsScreenState();
}

class _IdentityDetailsScreenState extends State<IdentityDetailsScreen> {
  Future<Map> identityDetails;
  Future<List> userPosts;
  Future<Map> userForum;
  bool alreadyFollowing = true;

  String mGroupId;

  @override
  void initState() {
    identityDetails = repo.getIdDetails(widget.args.identityId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,
      appBar: AppBar(
        title: Text("elRepo.io",
          style: TextStyle(color: WHITE_COLOR,),
        ),
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      body: identityDetailsWidget(),
    );
  }

  Widget identityDetailsWidget() {
    return Center(
      child: FutureBuilder(
          future: identityDetails,
          builder: (context, snapshot) {
            if (userForum == null) {
              userForum = repo.getUserForum(widget.args.identityId);
              userForum.then((forum) {
                //TODO(nicoechaniz): dirty implementation... without this check widget rebuilds constantly
                if (alreadyFollowing != (forum["mSubscribeFlags"] == 4)) {
                  setState(() {
                    alreadyFollowing = forum["mSubscribeFlags"] == 4;
                  });
                }
                if (alreadyFollowing) {
                  setState(() {
                    userPosts = repo.getForumMetadata(forum["mGroupId"]);
                    mGroupId = forum["mGroupId"];
                  });
                  userPosts.then((p) => print("These are the user posts: $p"));
                }
              });
            }
            if (snapshot.hasError) {
              print('Error: ${snapshot.error}');
              return Center(
                child: Text(IntlMsg.of(context).cannotLoadContent),
              );
            } else if (snapshot.hasData == false || snapshot.data.length == 0) {
              return loadingBox();
            } else {
              final details = snapshot.data;

              return new ListView(
                  children: <Widget>[
                new ListTile(
                    leading: Container(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Icon(
                          Icons.person,
                          color: NEUTRAL_COLOR,
                          size: 48,
                        )),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          details["mNickname"],
                          style: TextStyle(
                            fontSize: 24,
                            color: ACCENT_COLOR,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    //TODO make the follow button interactive
                    subtitle: Visibility(
                      visible: !alreadyFollowing,
                      child: RaisedButton(
                        //color: !alreadyFollowing ? LOCAL_COLOR : REMOTE_COLOR,
                        onPressed: () {
                          followThisUser();
                          new Text(IntlMsg.of(context).following);
                        },
                        child: Text(IntlMsg.of(context).follow),
                        textColor: Colors.white,
                        color: LOCAL_COLOR,
                      ),
                      replacement: RaisedButton(
                        child: Text(IntlMsg.of(context).following),
                        disabledTextColor: Colors.white,
                        disabledColor: REMOTE_COLOR,
                      ),
                    )),
                SizedBox(
                  height: 60,
                ),
                (userPosts != null)
                    ? new Container(
                        height: 420,
                        child: PostTeasersUpdatable(
                                generator: () => repo.getForumMetadata(mGroupId),
                            linkPosts: true))
                    : null
              ].where(notNull).toList());
            }
          }),
    );
  }

  void followThisUser() {
    userForum.then((forum) {
      repo.subscribeToForum(forum);
      setState(() {
        alreadyFollowing = true;
      });
      print("Subscribed to $forum");
    });
  }
}
