/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class CreateBackupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BackupDownloading();
  }
}

class BackupDownloading extends StatefulWidget {
  @override
  _BackupDownloadingState createState() => _BackupDownloadingState();
}

class _BackupDownloadingState extends State<BackupDownloading> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          brightness: Brightness.dark,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: WHITE_COLOR,
            ),
          ),
          title: Text(
            "Create a Backup",
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: WHITE_COLOR,
            ),
          ),
        ),
        body: Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 14),
            child: Center(
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(15.0),
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 32,
                      vertical: 12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Your account data will be copied to a zip file and you will be presented with a menu to share it."
                          " Choose a destination you consider secure and reliable."
                          "\n\nOnce the backup is shared you will need to log back in to elRepo.io"
                              "\n\nIf you ever need to restore this backup, you will need to download the zip file"
                              " and provide it in the sign-up screen.",
                          style: TextStyle(
                            color: REMOTE_COLOR,
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Divider(
                          height: 2,
                          color: Colors.grey,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        RaisedButton(
                          color: LOCAL_COLOR,
                          disabledColor: ACCENT_COLOR,
                          elevation: 4.0,
                          splashColor: REMOTE_COLOR,
                          onPressed: () async {
                            final sharedDirs =
                                await rs.RsFiles.getSharedDirectories();
                            if (sharedDirs.length > 0) {
                              final rsPath = await getRetrosharePath();
                              final rsDir = Directory(rsPath);
                              final appDocDir =
                                  await getApplicationDocumentsDirectory();

                              final backupZipPath = path.join(
                                  appDocDir.path, "elRepo_backup.zip");

                              await rscontrol.stopRetroshare();

                              try {
                                final zipFile = File(backupZipPath);
                                archive.ZipFile.createFromDirectory(
                                        sourceDir: rsDir,
                                        zipFile: zipFile,
                                        recurseSubDirs: true,
                                        includeBaseDirectory: false)
                                    .then((value) async {
                                  Share.shareFiles([backupZipPath],
                                      text: "elRepo.io backup zip file");
                                });
                              } catch (e) {
                                print(e);
                              }

                              await rscontrol.startRetroshare();
                              Navigator.pushReplacementNamed(context, '/login');
                            }
                          },
                          child: new Text(
                            "Create and share backup file",
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
