/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class OnBoarding extends StatefulWidget {
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  //one of the attempst to show of the linearprograssindicator
  //String creatingAccountMessage = Text('IntlMsg.of(context).creatingAccount');
  //bool _isLoading = false;

  //TODO validate the isCreatint account sttus and declare it here!
  final isCreating = repo.getAccountStatus();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: IntroductionScreen(
        pages: [
          PageViewModel(
              bodyWidget: Center(
                child: Text(
                  "bring down the cloud",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 19,
                      shadows: [
                        Shadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: 5,
                          offset: Offset(1, 1),
                        )
                      ]),
                ),
              ),
              image: Image.asset("assets/logo.png"),
              titleWidget: Text(
                "elRepo.io",
                style: TextStyle(fontSize: 30),
              )),
          PageViewModel(
              title: "Avoid censorship",
              bodyWidget: Center(
                child: Text(
                  "Share your content without depending on information silos controlled by global monopolies",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 19,
                      shadows: [
                        Shadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: 5,
                          offset: Offset(1, 1),
                        )
                      ]),
                ),
              ),
              image: Image.asset("assets/cloud.png", scale: 3)),
          PageViewModel(
              title: "Pop the information bubble",
              bodyWidget: Center(
                child: Text(
                  "What you search is what you get. No intrusive AI or algorithms will get between you and content shared by others",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 19,
                      shadows: [
                        Shadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: 5,
                          offset: Offset(1, 1),
                        )
                      ]),
                ),
              ),
              image: Image.asset("assets/search.png", scale: 3)),
          PageViewModel(
              title: "Local, global, online and offline",
              bodyWidget: Center(
                child: Text(
                  "Publish and view content even when you are not connected to the Internet.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 19,
                      shadows: [
                        Shadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: 5,
                          offset: Offset(1, 1),
                        )
                      ]),
                ),
              ),
              image: Image.asset("assets/message.png", scale: 3)),
          PageViewModel(
              title: "Welcome to elRepo.io glocal family!",
              bodyWidget: Center(
                child: Text(
                  "You can now begin sharing and exploring content.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 19,
                      shadows: [
                        Shadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: 5,
                          offset: Offset(1, 1),
                        )
                      ]),
                ),
              ),
              image: Image.asset("assets/checked.png", scale: 3))
        ],
        onDone: () {
          print("done");
        },
        showNextButton: true,
        nextFlex: 1,
        dotsFlex: 1,
        skipFlex: 1,
        animationDuration: 500,
        curve: Curves.fastOutSlowIn,
        dotsDecorator: DotsDecorator(
          spacing: EdgeInsets.all(5),
          activeColor: LOCAL_COLOR,
          activeSize: Size.square(11),
          size: Size.square(8),
        ),
        next: Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              border: Border.all(color: LOCAL_COLOR, width: 2)),
          child: Center(
            child: Icon(
              Icons.navigate_next,
              size: 30,
              color: ACCENT_COLOR,
            ),
          ),
        ),
        done: Container(
          height: 78,
          width: 78,
          child: ClipOval(
            child: new RaisedButton(
              color: ACCENT_COLOR,
              splashColor: REMOTE_COLOR,
              animationDuration: Duration(seconds:4),
              onPressed: () async {
                Navigator.pop(context);
              },
              child: Text(
                "Close",
                style: TextStyle(
                    color: WHITE_COLOR,
                    fontWeight: FontWeight.bold,
                    fontSize: 13.0,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
