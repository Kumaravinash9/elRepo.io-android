/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
 
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;

import '../../constants.dart';

class DownloadBar extends StatefulWidget {
  final String downloadingHash;
  final Function onDone;

  const DownloadBar({Key key, @required this.downloadingHash, this.onDone})
      : super(key: key);

  @override
  _DownloadBarState createState() => _DownloadBarState();
}

class _DownloadBarState extends  State<DownloadBar>  {
  String _downloadingHash;
  Timer _downloadProgressTimer;
  double downloadProgress = 0.0;
  Future<String> filePath;
  bool fileExists = false;
  double _progress;

  @override
  void initState() {
    _downloadingHash = widget.downloadingHash;
    startDownloadProgressTimer();
    super.initState();
  }

  @override
  void dispose() {
    _downloadProgressTimer?.cancel();
    super.dispose();
  }

  Future<void> startDownloadProgressTimer() async {
    var hashes = await rs.RsFiles.fileDownloads();
    if (hashes.length > 0) {
      for (var hash in hashes) {
        if (hash == _downloadingHash) {
          _progress = 0.0;
          _downloadProgressTimer = Timer.periodic(
            Duration(seconds: 1),
            _downloadProgressHandler,
          );
        }
      }
    }
  }

  void _downloadProgressHandler(Timer timer) async {
    var downloadDetails = await rs.RsFiles.fileDetails(_downloadingHash);
    _progress = downloadDetails["avail"]["xint64"] /
        downloadDetails["size"]["xint64"];
    print("progress for $_downloadingHash: $_progress");

    if (timer.isActive) {
      if (mounted) setState(() => downloadProgress = _progress);

      if (downloadProgress >= 1.0) {
        print("download finished.");
        downloadProgress = 1.0;

        // it takes a while for the downloaded file to be copied to it's final
        // location by Retroshare so we keep the timer up until it's done
        filePath = repo.getPathIfExists(_downloadingHash);
        filePath.then((path) async {
          // the file is already available on this device
          if (path != null) {
            timer.cancel();
            widget.onDone();
          }
        });
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    return LinearPercentIndicator(
      width: MediaQuery.of(context).size.width - 30,
      linearStrokeCap: LinearStrokeCap.round,
      lineHeight: 12.0,
      percent: downloadProgress,
      center: Text('Progress: ${(downloadProgress * 100).round()} %',
          style: new TextStyle(fontSize: 9.0, color: WHITE_COLOR)),
//linearStrokeCap: LinearStrokeCap.roundAll,
      backgroundColor: REMOTE_COLOR,
      progressColor: ACCENT_COLOR,
      animation: true,
      animationDuration: 2500,
    );
  }
}
