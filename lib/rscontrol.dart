/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter/services.dart';
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;
import 'utils.dart' as utils;

const rsPlatform = const MethodChannel(rs.RETROSHARE_CHANNEL_NAME);

void setControlCallbacks() {
  rs.setStartCallback(startRetroshare);
}

Future<bool> startRetroshare() async {
  int attempts = 20;
  for (; attempts >= 0; attempts--) {
    print("Starting Retroshare Service. Attempts countdown $attempts");
    try {
      bool isUp = await rs.isRetroshareRunning();
      if (isUp) return true;

      await rsPlatform.invokeMethod('start');

      if (attempts == 0) {
        return false;
      }
      await Future.delayed(Duration(seconds: 2));
    } catch (err) {
      await Future.delayed(Duration(seconds: 2));
    }
  }
}

Future<void> stopRetroshare() async {
  try {
    await rsPlatform.invokeMethod('stop');

    await Future.delayed(Duration(milliseconds: 3000));
    bool isUp = await rs.isRetroshareRunning();
    if (isUp) throw Exception("The service did not stop after a while");
  } catch (err) {
    throw Exception("The service could not be stopped");
  }
}

Future<void> restartRetroshare() async {
  try {
    await rsPlatform.invokeMethod('restart');

    await Future.delayed(Duration(milliseconds: 300));
    bool isUp = await rs.isRetroshareRunning();
    if (!isUp) throw Exception("The service did not restart after a while");
  } catch (err) {
    throw Exception("The service could not be restarted");
  }
}

Future<bool> prepareSystem( ) async {
  final downloadDir = await utils.getOrCreateExternalDir("/Download");
  final partialsDir = await utils.getOrCreateExternalDir("/Partials");
  return await repo.prepareSystem(downloadDir, partialsDir);
}

Future onlineMode() async {
  if (await rs.isRetroshareRunning()) {
    await rs.RsConfig.setMaxDataRates(10000, 10000);
  }
}

Future offlineMode() async {
  if (await rs.isRetroshareRunning()) {
    await rs.RsConfig.setMaxDataRates(500, 500);
  }
}

Future<String> getRetrosharePath() async {
  final appDocDir = await getApplicationDocumentsDirectory();
  final pathComponents = path.split(appDocDir.path);
  pathComponents.removeLast();
  final rsPath = path.join(path.joinAll(pathComponents), "files", ".retroshare");
  return rsPath;
}
