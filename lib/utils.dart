/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io' as Io;
import 'package:flutter/services.dart' as svc;
import 'package:permission_handler/permission_handler.dart' if (dart.library.cli_util) 'dummy.dart';
import 'package:path_provider/path_provider.dart' if (dart.library.cli_util) 'dummy.dart' ;
import 'package:connectivity/connectivity.dart';
import 'rscontrol.dart' as rsControl;

Future<String> getOrCreateExternalDir([String appendToPath = ""]) async {
  print("Getting or creating download folder.");
  final downloadPath = await getExternalPath() + appendToPath;
  if (await Permission.storage.request().isGranted) {
    await createDir(downloadPath);
    return downloadPath;
  } else {
//    openAppSettings();
    throw Exception("No permission to create the download directory.");
  }
}

Future<bool> createDir(String path) async {
  try {
    var dir = Io.Directory(path);
    bool dirExists = await dir.exists();
    if (!dirExists) {
      print("Creating directory: $path");
      await dir.create(recursive: true);
    }
  } catch (e) {
    print("Error creating directory. $e");
    return false;
  }
  return true;
}

Future<String> getExternalPath() async {
  List extDirectories = await getExternalStorageDirectories();
  Io.Directory externalPath = extDirectories.last;
  for (var extDir in extDirectories) {
    if (!extDir.path.contains('emulated')) {
      externalPath = extDir;
    }
  }
  return externalPath.path;
}

Future<void> initConnectivity() async {
  final Connectivity _connectivity = Connectivity();
  ConnectivityResult result = ConnectivityResult.none;
  try {
    result = await _connectivity.checkConnectivity();
  } on svc.PlatformException catch (e) {
    print(e.toString());
  }
  return updateConnectionStatus(result);
}

Future<void> updateConnectionStatus(ConnectivityResult result) async {
  if (result == ConnectivityResult.wifi) {
    rsControl.onlineMode();
  }
  else {
    rsControl.offlineMode();
  }
}

