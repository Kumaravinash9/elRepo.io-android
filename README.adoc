= elRepo.io-android

Android Client for elRepo.io


== Running

Copy local configuration example

[source,bash]
----
cd android
cp retroshare-service.properties.example retroshare-service.properties
cd ..
----

Edit `android/retroshare-service.properties` with your favorite editor
conforming to your development environement.

Attach your mobile phone to your PC and execute `flutter run` to enjoy testing
and developing the app.

If you get errors about NDK version not found you probably need to edit
`android/local.properties` setting `ndk.dir` properly. 
